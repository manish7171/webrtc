let divSelectRoom       =   document.getElementById('select-room');
let divConsultingRoom   =   document.getElementById('consulting-room');
let inputRoomNumber     =   document.getElementById('room-number');
let btnGoToRoom         =   document.getElementById('go-room');
let localVideo          =   document.getElementById('local-video');
let remoteVideo         =   document.getElementById('remote-video');
let h2CallName          =   document.getElementById('callName');
let inputCallName       =   document.getElementById('inputCallName');
let btnSetName          =   document.getElementById('setName');

let roomNumber, localStream, remoteStream, rtcPeerConnection, isCaller, dataChannel;

const iceServers = {
    'iceServer':[
        {'urls':'stun:stun.services.mozilla.com'},
        {'urls':'stun:stun.l.google.com:19302'}
    ]
};

const streamConstraints = {
    audio:true,
    video:true
}

const socket = io();

btnGoToRoom.onclick = () => {
    if(inputRoomNumber.value == ''){
        alert('Please type room name');
    }else{
        roomNumber = inputRoomNumber.value;
        socket.emit('create or join', roomNumber);
        /**/
        divSelectRoom.style = "display: none";
        divConsultingRoom.style = "display: block";
    }
};

btnSetName.onclick = () => {
    if(inputCallName.value == ''){
        alert('Please set name');
    }else{
        dataChannel.send(inputCallName.value);
        h2CallName.innerText = inputCallName.value;
    }
};

socket.on('created', room => {
    navigator.mediaDevices.getUserMedia(streamConstraints)
        .then(stream =>{
            localStream = stream;
            localVideo.srcObject = stream;
            isCaller = true;
        })
        .catch(err =>{
            console.log('An error occured',err);

        })
});

socket.on('joined', room => {
    navigator.mediaDevices.getUserMedia(streamConstraints)
        .then(stream =>{
            localStream = stream;
            localVideo.srcObject = stream;
            socket.emit('ready',roomNumber);
        })
        .catch(err =>{
            console.log('An error occured',err);

        })
});

socket.on('ready', () => {
    console.log('on ready isCaller',isCaller)
    if(isCaller){
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate;
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0],localStream); //video track
        rtcPeerConnection.addTrack(localStream.getTracks()[1],localStream); //audio track
        rtcPeerConnection.createOffer()
            .then(sessionDescription => {
                console.log('sending offer', sessionDescription);
                rtcPeerConnection.setLocalDescription(sessionDescription);
                socket.emit('offer',{
                    type:'offer',
                    sdp:sessionDescription,
                    room: roomNumber
                })
            })
            .catch(err => {
                console.log('sending offer err',err);
            })

        dataChannel = rtcPeerConnection.createDataChannel(roomNumber);
        dataChannel.onmessage = event => { h2CallName.innerText = event.data}
    }
});

socket.on('offer', (event) => {
    if(!isCaller){
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = onIceCandidate;
        rtcPeerConnection.ontrack = onAddStream;
        rtcPeerConnection.addTrack(localStream.getTracks()[0],localStream); //video track
        rtcPeerConnection.addTrack(localStream.getTracks()[1],localStream); //audio track
        console.log('recieved offer', event);

        rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));
        rtcPeerConnection.createAnswer()
            .then(sessionDescription => {
                console.log('sending answer', sessionDescription);
                rtcPeerConnection.setLocalDescription(sessionDescription);
                socket.emit('answer',{
                    type:'answer',
                    sdp:sessionDescription,
                    room: roomNumber
                })
            })
            .catch(err => {
                console.log('sending answer',err);
            })

        rtcPeerConnection.ondatachannel = event => {
            dataChannel = event.channel;
            dataChannel.onmessage = event => { h2CallName.innerText = event.data}

        }
    }
});

socket.on('answer',event => {
    console.log('received answer',event);
    rtcPeerConnection.setRemoteDescription(new RTCSessionDescription(event));

});

socket.on('candidate',event =>{
    const candidate = new RTCIceCandidate({
        sdpMLineIndex:event.label,
        candidate:event.candidate
    });
    console.log('received candidate',candidate)
    rtcPeerConnection.addIceCandidate(candidate);
})

function onAddStream(event) {
    remoteVideo.srcObject = event.streams[0]; //video stream
    remoteStream = event.streams[0];
}

function onIceCandidate(event) {
    if(event.candidate){
        console.log('sending ice candidate',event.candidate);
        socket.emit('candidate',{
            type:'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate,
            room:roomNumber
        });
    }else{
        console.log('sending ice candidate','no event')
    }
}

